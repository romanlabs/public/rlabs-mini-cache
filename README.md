<div style="text-align: center;">
<img src="images/logo.png" alt="Demo" width="300">
</div>

# Table of Contents
- [RLabs Mini Cache](#rlabs-mini-cache)
- [Installation](#installation)
- [Usage](#usage)
  - [Import](#import)
  - [Configure cache](#configure-cache)
  - [Getting a Cache Object](#getting-a-cache-object)
  - [Reading and writing from/to cache](#reading-and-writing-fromto-cache)
  - [Cache invalidation](#cache-invalidation)
- [Complete example](#complete-example)
  - [File backend example](#file-backend-example)
  - [MongoDB backend example](#mongodb-backend-example)
- [License](#license)
- [Copyright](#copyright)


# RLabs Mini Cache

RLabs Mini Cache is simple JSON caching library. Intended for quick prototyping.


# Installation

```bash
pip3 install rlabs-mini-cache
```

# Usage

### Import

```python
from rlabs_mini_cache.cache import Cache
```

### Configure cache

Mini cache has a built-in logger, it's possible to set the log level:

```python
Cache.config(
    log_level=logging.DEBUG
)
```

it also possible to override the built-in logger:

```python
Cache.config(
    logger_override=mylogger
)
```

### Getting a Cache Object

Cache objects operate on specific backends (memory, file, MongoDB), and they all take the following parameters:

- `read_fn: Callable`:
    - a function  of singnature (key: str) -> dict | list
    - this function will be called to fetch data (from source) on a cache miss
    - when calling `read_fn` the parameter `key` will be specified as a keyword argument
- `max_age: timedelta` :
    - the maximum age the data requested from cache can be
    - if the data requested from cache is older than `max_age` this is cache miss
      and new data will be read from source, stored, and returned.

This is a cache object for the memory backend:

```python
cache = Cache.Memory(
    read_fn=data_read,
    max_age=timedelta(seconds=3),
)
```

for the file backend:

```python
cache = Cache.File(
    read_fn=data_read,
    max_age=timedelta(seconds=60),
    dir_path='../.cache'
)
```

for the MongoDB backend:

```python
mongodb_client = MongoClient(
    MONGODB_URI,
    server_api=ServerApi('1'),
    tlsCAFile=certifi.where()
)

cache = Cache.MongoDB(
    read_fn=data_read
    max_age=timedelta(seconds=4),
    mongodb_client=mongodb_client,
    mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
)
```

### Reading and writing from/to cache

The main use case is reading:

```
cache.read('key')
```

but writing is also possible:

```
value: dict | list = ...
cache.write('key', value)
```

### Cache invalidation

Invalidating the cache results in the entire cache being deleted:

```
cache.invalidate()
```

# Complete example

### File backend example

```python
import logging
import time
from datetime import datetime

from rlabs_mini_cache.cache import Cache
from datetime import timedelta

SAMPLE_JSON = {
    "a": 'the time is ' + datetime.now().time().strftime("%H:%M:%S"),
    "b": "2",
    "c": "3",
}

def data_read_slow(
        key: str
    ):
    '''
        read
    '''
    time.sleep(5)
    return SAMPLE_JSON[key]

def main():
    '''
        main
    '''
    test_file_slow_backend()

def test_file_slow_backend():
    '''
        Test File Slow Backend
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.File(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=60),
        dir_path='../.cache'
    )

    print(
        #
        # this will be a cache miss
        # due to not being in cache
        #
        # SLOW WAIT
        #
        cache.read('a')
    )

    #
    # anything here below
    # will be a cache hit: FAST
    #
    print(
        cache.read('a')
    )
    print(
        cache.read('a')
    )

    #
    # Write a new value
    #
    cache.write(
        'a', 'the time is ' + datetime.now().time().strftime("%H:%M:%S")
    )

    #
    # anything here below
    # will be a cache hit: FAST
    #
    print(
        cache.read('a')
    )
    print(
        cache.read('a')
    )

if __name__ == '__main__':
    main()
```

this is the output from a second run (notice the initial cache hit, on the first run the initial read would be a miss; also notice that cache is initally loaded onto memory). 

![image](images/file.png)

cache contents:

![image](images/file_cache.png)

### MongoDB backend example

```python

import os
import logging
import time
from datetime import datetime
import certifi
from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi

from rlabs_mini_cache.cache import Cache
from datetime import timedelta

MONGODB_USER = os.environ['MONGODB_USER']
MONGODB_PASS = os.environ['MONGODB_PASS']
MONGODB_CLUSTER_DOMAIN_NAME = os.environ['MONGODB_CLUSTER_DOMAIN_NAME']
MONGODB_APP_NAME = os.environ['MONGODB_APP_NAME']
MONGODB_URI = (
    f"mongodb+srv://{MONGODB_USER}:{MONGODB_PASS}@{MONGODB_CLUSTER_DOMAIN_NAME}/"
    f"?appName={MONGODB_APP_NAME}&retryWrites=true&w=majority"
)

MONGODB_COLLECTION_NAME='rlabs_mini_cache_manual_test_run'

SAMPLE_JSON = {
    "a": 'the time is ' + datetime.now().time().strftime("%H:%M:%S"),
    "b": "2",
    "c": "3",
}

def data_read(
        key: str
    ):
    '''
        read
    '''
    return SAMPLE_JSON[key]

def main():
    '''
        main
    '''
    test_mongodb_backend()

def test_mongodb_backend():
    '''
        Test MongoDB Backend
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        max_age=timedelta(seconds=4),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
    )

    cache.read_fn = data_read

    print(
        #
        # this will be a cache miss
        # due to not being in cache
        # (we're invalidating at the end of the test)
        #
        cache.read('a')
    )
    time.sleep(1.1)
    print(
        cache.read('a')
    )
    time.sleep(1.1)
    print(
        cache.read('a')
    )
    time.sleep(1.1)
    print(
        #
        # this will be a cache miss
        # due to age
        #
        cache.read('a')
    )
    time.sleep(1.1)
    print(
        cache.read('a')
    )
    time.sleep(1.1)
    print(
        cache.read('a')
    )

    cache.invalidate()
```

The results (notice the `<< Cache Miss: Max Age Exceeded [Local Memory] >` int he middle due to the 4 seconds age specified):

![image](images/mongodb.png)

there's no cache contents because of the `invalidate` at the end, but if it were to be removed the output would look a tad different (notice the initial `<< Cache Hit [Remote MongoDB] >>`):

![image](images/mongodb2.png)

and the cache is preserved in MongoDB as a collection:

![image](images/mongodb_cache.png)


# License

This project is licensed under the GNU Lesser General Public License v3.0 - see the [LICENSE](./LICENSE) file for details.

# Copyright

Copyright (C) 2024 RomanLabs, Rafael Roman Otero
#
# Copyright (C) 2024 RomanLabs, Rafael Roman Otero
# This file is part of RLabs Mini Cache.
#
# RLabs Mini Cache is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RLabs Mini Cache is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with RLabs Mini Cache. If not, see <http://www.gnu.org/licenses/>.
#
'''
    Test rlabs_mini_cache
'''
import os
import pytest
import json
import logging
import time
from functools import wraps
import certifi
from pymongo.mongo_client import MongoClient
from pymongo.server_api import ServerApi

from rlabs_mini_cache.cache import Cache
from rlabs_mini_cache.cache import MongoDB
from datetime import timedelta

MONGODB_USER = os.environ['MONGODB_USER']
MONGODB_PASS = os.environ['MONGODB_PASS']
MONGODB_CLUSTER_DOMAIN_NAME = os.environ['MONGODB_CLUSTER_DOMAIN_NAME']
MONGODB_APP_NAME = os.environ['MONGODB_APP_NAME']
MONGODB_URI = (
    f"mongodb+srv://{MONGODB_USER}:{MONGODB_PASS}@{MONGODB_CLUSTER_DOMAIN_NAME}/"
    f"?appName={MONGODB_APP_NAME}&retryWrites=true&w=majority"
)
MONGODB_COLLECTION_NAME = 'rlabs_mini_cache_pytest'

SAMPLE_JSON = {
    "a": "1",
    "b": "2",
    "c": 3, # Intentional difference# mix in an int
}
DATA_READ_SLOW_SIMULATED_SLEEP: float = 1.0

def data_read_slow(
        key: str
    ):
    '''
        Simulate a slow read
    '''
    time.sleep(DATA_READ_SLOW_SIMULATED_SLEEP)
    return SAMPLE_JSON[key]

def data_read_fast(
        key: str
    ):
    '''
        Simulate a fast read
    '''
    return SAMPLE_JSON[key]

def timing_decorator(func):
    '''
        Timing decorator

        Times the execution of the decorated function

        Returns:
         (runtime, result)
    '''
    @wraps(func)
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()

        return end_time - start_time, result

    return wrapper

def test_read_fn_none():
    '''
        Test read_fn None
    '''
    Cache.config(
        log_level=logging.DEBUG
    )
    cache = Cache.Memory(
        max_age=timedelta(seconds=1),
        read_fn=None        #explicitly set to None
    )
    with pytest.raises(ValueError) as e:
        cache.read('a')

        assert 'read_fn' in str(e.value)

    cache = Cache.File(
        read_fn=None,
        max_age=timedelta(seconds=1),
        dir_path='../.cache'
        # read_fn not passed, implicitly set to None
    )
    with pytest.raises(ValueError) as e:
        cache.read('a')

        assert 'read_fn' in str(e.value)

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        max_age=timedelta(seconds=1),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME,
        # read_fn not passed, implicitly set to None
    )
    with pytest.raises(ValueError) as e:
        cache.read('a')

        assert 'read_fn' in str(e.value)


def test_backend_memory_cache_miss_not_found_cache_hit():
    '''
        Test Backend Memory - Cache miss not found, cache hit.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.Memory(
        read_fn=None,    # will set it later
        max_age=timedelta(seconds=1)
    )

    cache.read_fn = data_read_slow  # set it here

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    runtime1, val1 = cache_read('a')
    runtime2, val2 = cache_read('a')

    assert val1 == val2 == SAMPLE_JSON['a']
    assert runtime1 > runtime2

def test_backend_memory_max_age_zero():
    '''
        Test Backend Memory - Max age zero.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.Memory(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=0)
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.invalidate()
    runtime1, _ = cache_read('a')   # not in cache (not found)
    runtime2, _ = cache_read('a')   # not in cache (age excedeed)

    within_range = 0.1

    assert DATA_READ_SLOW_SIMULATED_SLEEP - runtime2 < within_range
    assert DATA_READ_SLOW_SIMULATED_SLEEP - runtime1 < within_range

def test_backend_memory_write():
    '''
        Test Backend Memory - Write
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.Memory(
        read_fn=data_read_fast,
        max_age=timedelta(seconds=1)
    )

    cache.write('a', True) # intentionally use a boolean
    val = cache.read('a')

    assert val == True

def test_backend_memory_write_and_invalidate():
    '''
        Test Backend Memory - Write and Invalidate
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.Memory(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1)
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.write('b', 7)
    runtime1, _ = cache_read('b')   # in cache
    runtime2, _ = cache_read('b')   # in cache

    cache.invalidate()
    runtime3, _ = cache_read('b')   # not in cache

    assert runtime3 > runtime1
    assert runtime3 > runtime2


def test_backend_file_cache_miss_not_found_cache_hit():
    '''
        Test Backend File - Cache miss not found, cache hit.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.File(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1),
        dir_path='../.cache'
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    runtime1, val1 = cache_read('a')
    runtime2, val2 = cache_read('a')

    assert val1 == val2 == SAMPLE_JSON['a']
    assert runtime1 > runtime2

def test_backend_file_cache_max_age_exceeded():
    '''
        Test Backend File - Cache max age exceeded.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.File(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1),
        dir_path='../.cache'
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.invalidate()
    runtime1, _ = cache_read('a')   # not in cache
    runtime2, _ = cache_read('a')   # in cache

    time.sleep(1) # let max age be exceeded

    runtime3, _ = cache_read('a')   # not in cache
    runtime4, _ = cache_read('a')   # in cache

    assert runtime1 > runtime2
    assert runtime1 > runtime4

    assert runtime3 > runtime2
    assert runtime3 > runtime4

def test_backend_file_keys_and_data_in_cache():
    '''
        Test Backend File - Keys and data in cache.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.File(
        read_fn=data_read_fast,
        max_age=timedelta(seconds=1),
        dir_path='../.cache'
    )

    #
    # after reading from cache
    #
    cache.read('a')
    cache.read('b')
    cache.read('c')

    with open(
            f'../.cache/{Cache.File.CACHE_FILENAME}',
            'r',
            encoding='utf-8'
        ) as f:
        raw_cache_data = json.load(f)

    assert raw_cache_data.keys() == SAMPLE_JSON.keys()

    for key, value in raw_cache_data.items():
        assert value['data'] == SAMPLE_JSON[key]

    #
    # after invalidate
    #
    cache.invalidate()

    with open(
            f'../.cache/{Cache.File.CACHE_FILENAME}',
            'r',
            encoding='utf-8'
        ) as f:
        raw_cache_data = json.load(f)

    assert raw_cache_data == {}

    #
    # after writing to cache
    #
    cache.write('a', "updated value a")
    cache.write('b', "updated value b")
    cache.write('c', "updated value c")

    with open(
            f'../.cache/{Cache.File.CACHE_FILENAME}',
            'r',
            encoding='utf-8'
        ) as f:
        raw_cache_data = json.load(f)

    assert raw_cache_data['a']['data'] == "updated value a"
    assert raw_cache_data['b']['data'] == "updated value b"
    assert raw_cache_data['c']['data'] == "updated value c"

def test_backend_file_write_and_invalidate():
    '''
        Test Backend File - Write and Invalidate
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    cache = Cache.Memory(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1)
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.write('b', 7)
    runtime1, _ = cache_read('b')   # in cache
    runtime2, _ = cache_read('b')   # in cache

    cache.invalidate()
    runtime3, _ = cache_read('b')   # not in cache

    assert runtime3 > runtime1
    assert runtime3 > runtime2

def test_backend_mongodb_cache_miss_not_found_cache_hit():
    '''
        Test Backend MongoDB - Cache miss not found, cache hit.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=60),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    runtime1, val1 = cache_read('a')  # not in local cache BUT MAY BE in MongoDB cache
                                      # either way should read from MongDB
    runtime2, val2 = cache_read('a')  # in cache (reads from local cache)

    assert val1 == val2 == SAMPLE_JSON['a']
    assert runtime1 > runtime2

def test_backend_mongodb_cache_max_age_exceeded():
    '''
        Test Backend MongoDB - Cache max age exceeded.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.invalidate()
    runtime1, _ = cache_read('a')   # not in cache
    runtime2, _ = cache_read('a')   # in cache (reads from local cache)

    time.sleep(1) # let max age be exceeded

    runtime3, _ = cache_read('a')   # not in cache
    runtime4, _ = cache_read('a')   # in cache (reads from local cache)

    assert runtime1 > runtime2
    assert runtime1 > runtime4

    assert runtime3 > runtime2
    assert runtime3 > runtime4

def test_backend_mongodb_keys_and_data_in_cache():
    '''
        Test Backend MongoDB - Keys and data in cache.
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        max_age=timedelta(seconds=1),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
        # read_fn not passed, implicitly set to None,
        # will set it later
    )

    cache.read_fn = data_read_slow  # set it here

    # start fresh
    cache.invalidate()

    #
    # after reading from cache
    #
    cache.read('a')
    cache.read('b')
    cache.read('c')

    collection = mongodb_client[MongoDB.CACHE_DB_NAME][MONGODB_COLLECTION_NAME]

    document_a = collection.find_one(
            {"index": 'a'}
    )
    document_b = collection.find_one(
            {"index": 'b'}
    )
    document_c = collection.find_one(
            {"index": 'c'}
    )

    assert document_a['data'] == SAMPLE_JSON['a']
    assert document_b['data'] == SAMPLE_JSON['b']
    assert document_c['data'] == SAMPLE_JSON['c']

    #
    # after invalidate
    #
    cache.invalidate()

    document_a = collection.find_one(
            {"index": 'a'}
    )
    document_b = collection.find_one(
            {"index": 'b'}
    )
    document_c = collection.find_one(
            {"index": 'c'}
    )

    assert not document_a
    assert not document_b
    assert not document_c

    #
    # after writing to cache
    #
    cache.write('a', "updated value a")
    cache.write('b', "updated value b")
    cache.write('c', "updated value c")

    document_a = collection.find_one(
            {"index": 'a'}
    )
    document_b = collection.find_one(
            {"index": 'b'}
    )
    document_c = collection.find_one(
            {"index": 'c'}
    )

    assert document_a['data'] == "updated value a"
    assert document_b['data'] == "updated value b"
    assert document_c['data'] == "updated value c"

def test_backend_mongodb_write_and_invalidate():
    '''
        Test Backend MongoDB - Write and Invalidate
    '''
    Cache.config(
        log_level=logging.DEBUG
    )

    mongodb_client = MongoClient(
        MONGODB_URI,
        server_api=ServerApi('1'),
        tlsCAFile=certifi.where()
    )

    cache = Cache.MongoDB(
        read_fn=data_read_slow,
        max_age=timedelta(seconds=1),
        mongodb_client=mongodb_client,
        mongodb_collection_unique_name=MONGODB_COLLECTION_NAME
    )

    @timing_decorator
    def cache_read(key: str):
        return cache.read(key)

    cache.invalidate()
    cache.write('b', 7)             # writes to both local AND MongoDB cache
    runtime1, _ = cache_read('b')   # in cache (reads from local cache)
    runtime2, _ = cache_read('b')   # in cache (reads from local cache)

    #
    # do it thrice to make sure it's not a fluke
    #
    # it's happened before that there was a bug in invalidate
    # where it was still reading from local (local was not invalidated)
    # but it'll pass half the times because they're both in cache
    #
    cache.invalidate()
    runtime3, _ = cache_read('b')   #  not in cache (reads from source)
    cache.invalidate()
    runtime4, _ = cache_read('b')   #  not in cache (reads from source)
    cache.invalidate()
    runtime5, _ = cache_read('b')   #  not in cache (reads from source)


    assert runtime3 > runtime2
    assert runtime3 > runtime1
    assert runtime4 > runtime2
    assert runtime4 > runtime1
    assert runtime5 > runtime1
    assert runtime5 > runtime2

# Changelog


## July 21, 2024 (version 1.0.3)
### Continuous integration
- **ci:** allow test faiures temporarily (944ed173)
- **ci:** migrate to new poetry package template ci (cb188212)

The update addresses the following issues:
- [Migrate All pipelines to new poetry CI template](https://gitlab.com/romanlabs/ci-templates/package-publish/-/issues/1)

Migrated pipeline to new release format